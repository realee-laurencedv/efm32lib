/*
	\name		ADCServer_conf.h
	\author		Laurence DV
	\date		2017-05-16
	\version	0.2.0
	\note		This is a template file with a working configuration
	\usage		See ADCServer-example.txt (not implemented yet)
	\warning	
*/
#ifndef ADCSERVER_CONF_H_
#define ADCSERVER_CONF_H_	1

#include <em_device.h>		/* For HW module addresses */
#include <em_adc.h>			/* For adcAcqTimennn definitions */
#include <em_cmu.h>			/* For cmuClock_nnn definitions */

/* -------------------------------- +
|									|
|	Configuration					|
|									|
+ ---------------------------------*/
#define ADCS_USE_CMSIS_RTOS			(1)				/* Define this if your project uses an CMSIS RTOS */

/* Memory related */
#define	ADCS_VERIFICATION_EN		(1)				/* 1: Enable multiple verifications and sanity check in all functions (useful in dev phase) */
#define ADCS_MAXNB_MODULE			(1)				/* Maximum number of module that will be used in parrallel (Currently all EFM32 families have only 1 ADC) */

/* Execution related */
#define ADCS_CONVRATE				(10000UL)		/* ADC Server conversion rate (in Sample per sec) */
#define	ADCS_MONRATE				(1000UL)		/* Conversion rate for the monitored internal channels (Should ALWAYS be less than ADCS_CONVRATE) */
#define ADCS_SCAN_CH_NB				(1)				/* Total number of single-end channel (or pair of diff channel) that will be converted (max: 8) */
#define ADCS_MON_CH_NB				(3)				/* Total number of channel monitored (max: 15) */

/* Converter timings */
#define	ADCS_CONVCLK				(10000000UL)	/* ADC internal clock (in Hz) */
#define	ADCS_ACQCYCLENB				(adcAcqTime128)	/* Number of acquisition cycles for the external input */
#define	ADCS_MON_ACQCYCLENB			(adcAcqTime16)	/* Number of acquisition cycles for the monitored internal channels */


/* -------------------------------- +
|									|
|	Hardware definition				|
|									|
+ ---------------------------------*/
#define	ADCS_ADC_MOD				(ADC0)
#define	ADCS_ADC_CLK				(cmuClock_ADC0)

#define ADCS_TMR_MOD				(TIMER1_BASE)
#define	ADCS_TMR_CLK				(cmuClock_TIMER1)

#define	ADCS_DMA_CH					(2)

#define	ADCS_PRS_CH					3				/* PRS channel used for the ADCServer (NOTE: do not use "()", ie: for Ch0:   0 ) */

#endif /* ADCSERVER_CONF_H_ */
