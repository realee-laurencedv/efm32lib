/*
	\name		Megaxone's Debugging tools
	\author		Laurence DV
	\date		2017-05-23
	\version	0.3.0
	\usage		This is a template config file for mxbdg, make a local copy in your project and include it
	\warning	
*/
#ifndef MXDBG_CONF_H_
#define MXDBG_CONF_H_	1

/* -------------------------------- +
|	Config							|
+ ---------------------------------*/
#define	MXDBG_PORT_SEL			MXDBG_PORT_SWO		/* Select the type of interface (use MXDBG_PORT_xxx in mxdbg.h) */

/* GPIO & DDP config */
#define MXDBG_GPIO_DATAPORT		(2)					/* Port number of the GPIO interface (A: 0, B: 1, C: 2, D: 3, E: 4, F: 5, G: 6) only valid if MXDBG_PORT_SEL is GPIO or SOFTUART */
#define MXDBG_GPIO_DATAMASK		(0x0078)			/* Mask of usable pin of the GPIO interface */
#define MXDBG_GPIO_DATAWIDTH	(4)					/* Width of the data part of the interface */
#define MXDBG_GPIO_DATAALIGN	(3)					/* Pin number of the first pin in the GPIO interface */
#define MXDBG_GPIO_SYNC			(1)					/* Define if the GPIO interface is synchronous (1) or Asynchronous (0) */
#define MXDBG_GPIO_SYNCPORT		(2)					/* Port number of the GPIO Sync pin(s) */
#define	MXDBG_GPIO_SYNCMASK		(0x0001)			/* Mask for the sync pin(s) */
#define MXDBG_GPIO_SYNCEDGE		(MXDBG_GPIO_EDGEPOL_BOTH)	/* Edge polarity for the sync signal */

/* Soft UART config */
#define MXDBG_SOFTUART_TXPIN	(0)					/* Pin number of the TX pin for the Soft UART */

#endif /* MXDBG_CONF_H_ */
