/*
	\name		LEDmux_conf.h
	\author		Laurence DV
	\date		2017-08-15
	\version	0.1.0
	\note		This is a template file with a working configuration
	\usage		
	\warning	
*/
#ifndef LEDMUX_CONF_H_
#define LEDMUX_CONF_H_	1

#include <em_device.h>		/* for HW module addresses */
#include <em_gpio.h>		/* for gpioPort definition */

/* -------------------------------- +
|									|
|	Configuration					|
|									|
+ ---------------------------------*/
#define	LEDMUX_LEDNB				(2)			/* Number of LEDs multiplexed */

#define	LEDMUX_PWMFREQ_HZ			(10000)		/* PWM frequency of the colors(in Hz) */
#define	LEDMUX_FREQ_HZ				(1000)		/* Muxltiplexing frequency (in Hz) */
#define	LEDMUX_BLINK_HZ				(2)			/* Displayed blinking frequency (in Hz) */

/* -------------------------------- +
|									|
|	Hardware definition				|
|									|
+ ---------------------------------*/
											/*	Red				Green			Blue		*/
static const uint8_t	LEDMUXcolorPins[3] = {	0,				1,				2			};	/* Pin number of the colors terminals of the LEDs */
static const uint8_t	LEDMUXcolorPorts[3] = {	gpioPortA,		gpioPortA,		gpioPortA	};	/* Port number of the colors terminals of the LEDs */
static const uint8_t	LEDMUXcommonPins[LEDMUX_LEDNB] =	{	0,			1			};	/* Pin number of the common terminals of the LEDs */
static const uint8_t	LEDMUXcommonPorts[LEDMUX_LEDNB] =	{	gpioPortB,	gpioPortB	};	/* Port number of the common terminals of the LEDs */

#define	LEDMUX_COMMON_ACTIVE		(1)						/* Pin value to activate the common therminal of the LED */
#define	LEDMUX_COLOR_ACTIVE			(0)						/* Pin value to active a color terminal of the LED */

#define	LEDMUX_TMR_MOD				(TIMER0)					/* Hardware module handle */
#define	LEDMUX_TMR_LOC				(TIMER_ROUTE_LOCATION_LOC0)	/* Hardware location to use */
#define	LEDMUX_TMR_CLK				(cmuClock_TIMER0)
#define	LEDMUX_TMR_IRQ				TIMER0_IRQn
#define	LEDMUX_TMR_CCEN				((TIMER_ROUTE_CC0PEN)|(TIMER_ROUTE_CC1PEN)|(TIMER_ROUTE_CC2PEN))
#define	LEDMUX_RED_CC				(0)						/* Red color Compare Channel */
#define	LEDMUX_GREEN_CC				(1)						/* Green color Compare Channel */
#define	LEDMUX_BLUE_CC				(2)						/* Blue color Compare Channel */

#endif /* LEDMUX_CONF_H_ */
