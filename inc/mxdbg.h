/*
	\name		Megaxone's Debugging tools
	\author		Laurence DV
	\date		2017-05-23
	\version	0.3.0
	\note		Lowest level debug port for system diagnostic
				The Direct Debug Macro can be used when an other type of interface the "GPIO" is selected, but you must 
				ensure that MXDBG_GPIO_DATAPORT, MXDBG_GPIO_DATAMASK and MXDBG_GPIO_DATAALIGN is correctly set.
				GPIO interface only support contiguous pin (for now) and send data in Most-significance first
				GPIO Async: interleave 0 between part of data and leave the lines at 0 after printing
				GPIO Sync: data is valid on both edge of the MXDBG_GPIO_SYNCMASK pins
				To disable all function (except "exception handler") just set MXDBG_PORT_SEL in mxdbg_conf.h to MXDBG_PORT_NONE

	\warning	The SWO interface uses high-priority interrupt to transfert data (it can mess with execution timing)
*/
#ifndef MXDBG_H_
#define MXDBG_H_	1

/* -------------------------------- +
|									|
|	Include							|
|									|
+ ---------------------------------*/
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <em_chip.h>
#include <mxutil.h>
#if	defined _EFM32_GECKO_FAMILY		||\
	defined _EFM32_GIANT_FAMILY		||\
	defined _EFM32_LEOPARD_FAMILY	||\
	defined _EFM32_JADE_FAMILY		||\
	defined _EFM32_TIGER_FAMILY
	#define _MXFBG_CORE_ACCESS
	#include <core_cm3.h>
#elif defined _EFM32_WONDER_FAMILY || defined _EFM32_PEARL_FAMILY
	#define _MXFBG_CORE_ACCESS
	#include <core_cm4.h>
#endif


/* -------------------------------- +
|									|
|	Config values					|
|									|
+ ---------------------------------*/
/* Supported port */
#if defined _EFM32_HAPPY_FAMILY || defined _EFM32_ZERO_FAMILY
	#define	MXDBG_PORT_NONE		1
	#define	MXDBG_PORT_GPIO		2
	#define MXDBG_PORT_UART		4
	#define MXDBG_PORT_SOFTUART	8
#elif	defined _EFM32_GECKO_FAMILY		||\
		defined _EFM32_GIANT_FAMILY		||\
		defined _EFM32_LEOPARD_FAMILY	||\
		defined _EFM32_JADE_FAMILY		||\
		defined _EFM32_TIGER_FAMILY		||\
		defined _EFM32_WONDER_FAMILY	||\
		defined _EFM32_PEARL_FAMILY
	#define	MXDBG_PORT_NONE		1
	#define	MXDBG_PORT_GPIO		2
	#define MXDBG_PORT_UART		4
	#define MXDBG_PORT_SOFTUART	8
	#define MXDBG_PORT_SWO		16
#endif

/* Supported GPIO DATAVALID edge type */
#define MXDBG_GPIO_EDGEPOL_BOTH	0
#define MXDBG_GPIO_EDGEPOL_RISN	1
#define MXDBG_GPIO_EDGEPOL_FALL	2

#include <mxdbg_conf.h>

#ifndef MXDBG_PORT_SEL
	#error "MXDBG_PORT_SEL not defined, refer to \"setup\" section of readme"
#endif


/* -------------------------------- +
|									|
|	Direct Debug Port				|
|									|
+ ---------------------------------*/
#define DBGSET(mask)				(_DDPREG_SET((mask)<<MXDBG_GPIO_DATAALIGN))
#define DBGCLR(mask)				(_DDPREG_CLR((mask)<<MXDBG_GPIO_DATAALIGN))
#define DBGTOG(mask)				(_DDPREG_TOG((mask)<<MXDBG_GPIO_DATAALIGN))
#define DBGPULSE(mask)				(DBGTOG(mask), DBGTOG(mask))


/* -------------------------------- +
|									|
|	API Function					|
|									|
+ ---------------------------------*/
/*
	\brief	Initialize the debug output to a known and working state
	\note	Directly after the execution of this function the stdOutput will start working
	\param	void
	\return	void
*/
void MXDBG_Init(void);

/*
	\brief	Output the code on Debug port with a configurable number of bit
	\note	
	\param	uint32_t code		Code value to print
			uint32_t codeSize	Code width in bit number (valid: 0-32)
	\return	void
*/
void MXDBG_Code(uint32_t code, uint32_t codeSize);

/*
	\brief	Output a byte on the debug output port
	\note	
	\param	uint8_t outValue	Value to print
	\return	void
*/
void MXDBG_U8(uint8_t outValue);

/*
	\brief	Output a 16bit on the debug output port
	\note	
	\param	uint16_t outValue	Value to print
	\return	void
*/
void MXDBG_U16(uint16_t outValue);

/*
	\brief	Output a 32bit on the debug output port
	\note	
	\param	uint32_t outValue	Value to print
	\return	void
*/
void MXDBG_U32(uint32_t outValue);

/*
	\brief	Output an array of bytes on the debug output port
	\note	This is a wrapper using MXDBG_Print8()
	\param	uint8_t * arrayPtr	Start address of the array to output
	\param	uint32_t byteNb		Size of the array (in bytes)
	\return	void
*/
void MXDBG_Array(uint8_t * arrayPtr, uint32_t byteNb);

/*
	\brief	Output a null terminated string on the debug output port
	\note	This is a wrapper using MXDBG_Print8()
	\param	const char * string		string to print
	\return	void
*/
void MXDBG_String(const char * string);


/* -------------------------------- +
|									|
|	Direct Function					|
|									|
+ ---------------------------------*/
/*
	\brief	Specific mode init function
	\note	Those function are used by MXBDG_Init(), but you can call them directly if
			you need multiple debug system at the same time
	\param	void
	\return	void
*/
void MXDBG_InitGPIO(void);
void MXDBG_InitSWO(void);

/*
	\brief	Output the code on Debug port with a configurable number of bit
	\note	maximum codeSize is 16 because of GPIO register limitation
	\param	uint16_t code		Code value to print
			uint8_t codeSize	Code width in bit number (valid: 0-16)
	\return	void
*/
void MXDBG_PrintGPIO(uint16_t code, uint8_t codeSize);

/*
	\brief	Output the code via the SWO pin
	\note	
	\param	uint32_t code		Code value to print
	\return	void
*/
void MXDBG_PrintSWO(uint32_t code);




#endif /* MXDBG_H_ */
