/*
	\name		ADCUtil.h
	\author		Laurence DV
	\date		2017-05-26
	\version	0.1.1
	\note		Various utilities for adc input, joystick and cie.
	\usage		Use any calibration function before using the getter function
	\warning	Uses some floating-point math (for now)
*/
#ifndef ADCUTIL_H_
#define ADCUTIL_H_	1

/* -------------------------------- +
|									|
|	Include							|
|									|
+ ---------------------------------*/
#include <stdlib.h>
#include <stdint.h>

/* Debugging */
#include <mxdbg.h>

/* EFM32 Specific */


/* -------------------------------- +
|									|
|	Constant						|
|									|
+ ---------------------------------*/


/* -------------------------------- +
|									|
|	Type							|
|									|
+ ---------------------------------*/
typedef enum {
	ADCU_success =		0,			/* No error */
	ADCU_err_fail =		-1,			/* General error */
	ADCU_err_null =		-2,			/* NULL pointer deferencing */
	ADCU_err_memory =	-3,			/* General memory error (allocation/free) */
	ADCU_err_space =	-4,			/* Not enough space */
	ADCU_err_invalid =	-5,			/* Invalid parameter */
	ADCU_err_uncalib =	-6,			/* Not calibrated */
}ADCU_err_t;

typedef enum {
	ADCU_calNone =		0,
	ADCU_cal2point =	2,
	ADCU_cal3point =	3,
}ADCU_calibType_t;

typedef enum {
	ADCU_stInit =		0,
	ADCU_stReady =		1,
}ADCU_calibState_t;

/* Joystick container */
typedef struct {
	int16_t					value;			/* Calibrated value of the joystick (that is what you read) */
	struct {
		float				_coef1;			/* Internal use coefficient1 */
		float				_coef2;			/* Internal use coefficient2 */
		uint16_t			_coef3;			/* Internal use coefficient3 */
		ADCU_calibType_t	type;			/* Calibration type selected */
		ADCU_calibState_t	state;			/* Should be ADCU_stReady before accessing "value" */
	}calib;
}ADCU_joystick_t;

/* -------------------------------- +
|									|
|	Prototype						|
|									|
+ ---------------------------------*/
/*
	\brief	Calibrate a joystick with 2 points (min & max)
	\note
	\param	ADCU_joystick_t * joystick	Joystick which need to be calibrated
			uint16_t minValue			Minimum value of the joystick axis (lower-bound)
			uint16_t maxValue			Maximum value of the joystick axis (upper-bound)
			uint16_t outFullScale		Output upper-bound value (used to scale the input)
	\return	ADCU_err_t status			see ADCU_err_t
*/
ADCU_err_t ADCU_joyCalib2pts(ADCU_joystick_t * joystick, uint16_t minValue, uint16_t maxValue, uint16_t outFullScale);

/*
	\brief	Calibrate a joystick with 3 points (min, mid & max)
	\note
	\param	ADCU_joystick_t * joystick	Joystick which need to be calibrated
			uint16_t minValue			Minimum value of the joystick axis (lower-bound)
			uint16_t midValue			Middle value of the joystick axis (resting point)
			uint16_t maxValue			Maximum value of the joystick axis (upper-bound)
			uint16_t outFullScale		Output upper-bound value (used to scale the input)
	\return	ADCU_err_t status			see ADCU_err_t
*/
ADCU_err_t ADCU_joyCalib3pts(ADCU_joystick_t * joystick, uint16_t minValue, uint16_t midValue, uint16_t maxValue, uint32_t outFullScale);

/*
	\brief	Update the value of a joystick with the new rawValue
	\note
	\param	ADCU_joystick_t * joystick	Joystick to update
			uint16_t rawValue			new ADC raw value
	\return	ADCU_err_t status			see ADCU_err_t
*/
ADCU_err_t ADCU_joyUpdate(ADCU_joystick_t * joystick, uint16_t rawValue);

/*
	\brief	Return the last calibrated value
	\note	Simple getter with some verification
			You can also access joystick.value directly if you need speed
	\param	ADCU_joystick_t * joystick	Joystick to get
	\return	int16_t
*/
int16_t ADCU_joyGet(ADCU_joystick_t * joystick);

#endif /* ADCUTIL_H_ */
