/*
	\name		ADCServer.h
	\author		Laurence DV
	\date		2017-05-16
	\version	0.2.0
	\note		OS Hardware-running service used to get ADC fixed freq sampling
				This lib is thread-safe (tested with RTX v.4.70)
	\usage		See ADCServer-example.txt (not implemented yet)
	\warning	
*/
#ifndef ADCSERVER_H_
#define ADCSERVER_H_	1

/* -------------------------------- +
|									|
|	Include							|
|									|
+ ---------------------------------*/
#include <stdlib.h>
#include <stdint.h>
#include <mxutil.h>

/* Configuration */
#include <ADCServer_conf.h>
#if defined ADCS_USE_CMSIS_RTOS
#include <cmsis_os.h>				/* Important to include if inside an CMSIS RTOS (will use RTOS services) */
#endif

/* Debugging */
#include <mxdbg.h>

/* EFM32 Specific */
#include <em_device.h>
#include <em_core.h>
#include <em_cmu.h>
#include <em_dma.h>
#include <em_prs.h>
#include <em_adc.h>
#include <em_timer.h>


/* -------------------------------- +
|									|
|	Constant						|
|									|
+ ---------------------------------*/
/* Channels index */
#define ADCS_CH0				(_ADC_SINGLECTRL_INPUTSEL_CH0)
#define	ADCS_CH1				(_ADC_SINGLECTRL_INPUTSEL_CH1)
#define	ADCS_CH2				(_ADC_SINGLECTRL_INPUTSEL_CH2)
#define	ADCS_CH3				(_ADC_SINGLECTRL_INPUTSEL_CH3)
#define	ADCS_CH4				(_ADC_SINGLECTRL_INPUTSEL_CH4)
#define	ADCS_CH5				(_ADC_SINGLECTRL_INPUTSEL_CH5)
#define	ADCS_CH6				(_ADC_SINGLECTRL_INPUTSEL_CH6)
#define	ADCS_CH7				(_ADC_SINGLECTRL_INPUTSEL_CH7)
#define	ADCS_TEMP				(_ADC_SINGLECTRL_INPUTSEL_TEMP)
#define	ADCS_VDDDIV3			(_ADC_SINGLECTRL_INPUTSEL_VDDDIV3)
#define	ADCS_VDD				(_ADC_SINGLECTRL_INPUTSEL_VDD)
#define	ADCS_VSS				(_ADC_SINGLECTRL_INPUTSEL_VSS)
#define	ADCS_VREF				(_ADC_SINGLECTRL_INPUTSEL_VREFDIV2)
#define	ADCS_DAC0				(_ADC_SINGLECTRL_INPUTSEL_DAC0OUT0)
#define	ADCS_DAC1				(_ADC_SINGLECTRL_INPUTSEL_DAC0OUT1)

/* -------------------------------- +
|									|
|	Type							|
|									|
+ ---------------------------------*/
typedef enum {
	ADCS_success =			0,			/* No error */
	ADCS_err_fail =			-1,			/* General error */
	ADCS_err_null =			-2,			/* NULL pointer deferencing */
	ADCS_err_memory =		-3,			/* General memory error (allocation/free) */
	ADCS_err_space =		-4,			/* Not enough space */
	ADCS_err_invalid =		-5,			/* Invalid parameter */
	ADCS_err_chNotFound =	-6,			/* ADC Channel not found */
}ADCS_err_t;

typedef enum {
	ADCS_mon_normal =		0,			/* Channel is currently not in alarm */
	ADCS_mon_alarmLow =		1,			/* Channel is currently in lower-bound alarm */
	ADCS_mon_alarmHigh =	2,			/* Channel is currently in high-bound alarm */
}ADCS_MonState_t;

typedef struct ADCS_MonChStruct {
	ADCS_MonState_t			state;			/* Current state of the channel */
	uint8_t					ID;				/* Analog input channel index */
	uint16_t				raw;			/* Buffer for monitor mode */
	uint16_t				min;			/* Lower-bound (below which the ADCS call the cbFunction) */
	uint16_t				max;			/* Upper-bound (above which the ADCS call the cbFunction) */
	void(*cb)(uint16_t);					/* Callback functions for monitoring */
}ADCS_MonCh_t;

/* ADC module handle structure */
typedef struct ADCS_ModuleStruct {
	struct {
		uint16_t		ref_mV;					/* Voltage of the Full-scale reference (in mV) */
		ADCS_MonCh_t	ch[ADCS_MON_CH_NB];		/* List of monitored channel */
		uint8_t			curID;					/* Index of the currently converting channel */
		uint8_t			enCnt;					/* Current number of enabled channel */
		uint32_t		rateCnt;				/* Monitor rate counter */
	}mon;
	struct {
		uint16_t		ref_mV;					/* Voltage of the Full-scale reference (in mV) */
		uint16_t		raw[ADCS_SCAN_CH_NB];	/* Buffer for scan mode */
	}scan;
}ADCS_t;

/* -------------------------------- +
|									|
|	Prototype						|
|									|
+ ---------------------------------*/
/*
	\brief	Initialize the ADC to be used in single or scan mode
	\note	Directly after the execution of this function the ADC is ready to convert
	\usage	
	\param	uint32_t scanChannels			Analog Inputs to enable for the scan mode
			
	\return	ADCS_t * handle					ADCServer control structure handle
*/
ADCS_t * ADCS_Init(uint32_t scanChannels);

/*
	\brief	Disable and return the ADC to it's POR state, delete it's handle
	\note	Useful to reduce memory footprint
	\param	ADCS_t * handle			ADCServer control structure handle
	\return	ADCS_err_t errorCode	see ADCS_err_t
*/
ADCS_err_t ADCS_Remove(ADCS_t * handle);

/*
	\brief	Start/Stop the ADCServer's conversion and monitoring
	\note	The ADCServer must be initialized first (with ADCS_Init())
	\param	ADCS_t * handle			ADCServer control structure handle
	\return	ADCS_err_t errorCode	see ADCS_err_t
*/
ADCS_err_t ADCS_Start(ADCS_t * handle);
ADCS_err_t ADCS_Stop(ADCS_t * handle);

/*
	\brief	Add a callback which will be called when the specified input goes out-of-bound
	\note	The ADCServer will call the specified function with the triggering value passed
			as it's single parameter, it is up to the function to determine if it was over or
			under value that cause the call.
			Comparison is "<" or ">" so if you need only maxValue to trig, set minValue to 0
			If NULL is passed as the cbFunction, the monitoring will be active,
			but only available via getter function
	\warn	The callback function will be executed in interrupt context!
	\param	uint8_t channel			Associated analog input (use "Channels index" defines ie: ADCS_CH0)
			uint16_t minValue		Lower bound (below which the ADCS call the cbFunction)
			uint16_t maxValue		Upper bound (above which the ADCS call the cbFunction)
			void(*cbFunction)(uint16_t)	Callback function to call when the conversion result is out-of-bound
	\return	ADCS_err_t errorCode	see ADCS_err_t
*/
ADCS_err_t ADCS_MonAdd(uint8_t channel, uint16_t minValue, uint16_t maxValue, void(*cbFunction)(uint16_t));

/*
	\brief	Change the comparaison level for an already monitored channel
	\note	
	\param	uint8_t channel			Associated analog input (use "Channels index" defines ie: ADCS_CH0)
			uint16_t minValue		Lower bound (below which the ADCS call the cbFunction)
			uint16_t maxValue		Upper bound (above which the ADCS call the cbFunction)
	\return	ADCS_err_t errorCode	see ADCS_err_t
*/
ADCS_err_t ADCS_MonChangeLvl(uint8_t channel, uint16_t minValue, uint16_t maxValue);

/*
	\brief	Return the last value converted for the specified scanned channel
	\note	
	\param	uint8_t channel			Channel id
	\return	uint16_t value			Last conversion result
*/
uint16_t ADCS_ScanGetRaw(uint8_t channel);

/*
	\brief	Return the last value converted for the specified monitored channel
	\note
	\param	uint8_t channel			Channel id
	\return	uint16_t value			Last conversion result
*/
uint16_t ADCS_MonGetRaw(uint8_t channel);


#warning "TODO Comment header for ADCS_xxxGetmV()"
uint32_t ADCS_MonGetmV(uint8_t channel);
uint32_t ADCS_ScanGetmV(uint8_t channel);

/*
	\brief	Return the current core temp in deg Celcius
	\note
	\return	float temp				Current core temperature
*/
float ADCS_TempGet(void);

/*
	\brief	Return the current VDD voltage in mV
	\note
	\return	float vddValue			Current VDD voltage
*/
uint16_t ADCS_VDDGet_mV(void);

/*
	\brief	ADC Interrupt Handler
	\note	Everything needed to send bytes is handled inside this function
	\usage	Call this function from the real ISR with the correct module address
			For faster access, inline it in the ISR
	\param	ADCS_t * handle		ADCServer control structure handle
	\return	void
*/
void ADCS_ADC_ISR(ADCS_t * handle);


#endif /* ADCSERVER_H_ */
