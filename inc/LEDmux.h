/*
	\name		LEDmux.h
	\author		Laurence DV
	\date		2017-08-15
	\version	0.1.0
	\note		RGB LED multiplexer
				Maximum of 16 muxed LEDs
				Needs an CMSIS RTOS
	\usage		
	\warning	Color terminals must be on a separate GPIO port than the common terminals, because the current limit is port-based, not pin-based.
*/
#ifndef LEDMUX_H_
#define LEDMUX_H_	1

/* -------------------------------- +
|									|
|	Include							|
|									|
+ ---------------------------------*/
#include <stdlib.h>
#include <stdint.h>
#include <cmsis_os.h>
#include <LEDmux_conf.h>

/* Debugging */
#include <mxdbg.h>

/* EFM32 Specific */
#include <em_core.h>
#include <em_cmu.h>
#include <em_gpio.h>
#include <em_timer.h>

/* -------------------------------- +
|									|
|	Constant						|
|									|
+ ---------------------------------*/


/* -------------------------------- +
|									|
|	Type							|
|									|
+ ---------------------------------*/
typedef union {
	uint32_t all;
	struct {
		uint8_t	blinkEn:1;		/* Blink enable flag */
		uint8_t	:3;
		uint8_t LEDsel:4;		/* LED selection id */

		uint8_t red;			/* Red color value */
		uint8_t	green;			/* Green color value */
		uint8_t	blue;			/* Blue color value */
	};
}LEDMUXmsg_t;

typedef enum {
	LEDMUXsig_frameDone =	0x0001,
	LEDMUXsig_selfTest =	0x0002,	/* NOT IMPLEMENTED */
}LEDMUXsig_t;


/*--------------------------------- +
|									|
|	Global Variable					|
|									|
+ ---------------------------------*/
osMessageQId LEDMUXmsgBox;		/* LEDmux input message box (use LEDMUXmsg_t) */


/* -------------------------------- +
|									|
|	Prototype						|
|									|
+ ---------------------------------*/
/*
	\brief	LED Multiplexer thread function
	\note	
	\usage	
	\param	void	(still need to respect the CMSIS standard)
	\return	void
*/
void LEDMUXThread(void const *argument);

/*
	\brief	Timer Interrupt Handler
	\note	
	\usage	Call this function from the real ISR with the correct module address
			For faster access, inline it in the ISR
	\param	void
	\return	void
*/
void LEDMUX_ISR(void);

/* -------------------------------- +
|									|
|	Macro							|
|									|
+ ---------------------------------*/


#endif /* LEDMUX_H_ */
