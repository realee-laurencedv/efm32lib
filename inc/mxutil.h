/*
*	\name		util.h
*	\author		Laurence DV
*	\date		2017-08-15
*	\version	1.1.0
*	\brief		General utilities and macro tools
*	\note		
*	\warning	None
*/

#ifndef MXUTIL_H_
#define MXUTIL_H_

/* -------------------------------- +
|									|
|	include							|
|									|
+ ---------------------------------*/


/* -------------------------------- +
|									|
|	Define							|
|									|
+ ---------------------------------*/
#define	BIT0		(1 << 0)
#define	BIT1		(1 << 1)
#define	BIT2		(1 << 2)
#define	BIT3		(1 << 3)
#define	BIT4		(1 << 4)
#define	BIT5		(1 << 5)
#define	BIT6		(1 << 6)
#define	BIT7		(1 << 7)
#define	BIT8		(1 << 8)
#define	BIT9		(1 << 9)
#define	BIT10		(1 << 10)
#define	BIT11		(1 << 11)
#define	BIT12		(1 << 12)
#define	BIT13		(1 << 13)
#define	BIT14		(1 << 14)
#define	BIT15		(1 << 15)
#define	BIT16		(1 << 16)
#define	BIT17		(1 << 17)
#define	BIT18		(1 << 18)
#define	BIT19		(1 << 19)
#define	BIT20		(1 << 20)
#define	BIT21		(1 << 21)
#define	BIT22		(1 << 22)
#define	BIT23		(1 << 23)
#define	BIT24		(1 << 24)
#define	BIT25		(1 << 25)
#define	BIT26		(1 << 26)
#define	BIT27		(1 << 27)
#define	BIT28		(1 << 28)
#define	BIT29		(1 << 29)
#define	BIT30		(1 << 30)
#define	BIT31		(1 << 31)
#define	BIT32		(1 << 32)
#define	BIT33		(1 << 33)
#define	BIT34		(1 << 34)
#define	BIT35		(1 << 35)
#define	BIT36		(1 << 36)
#define	BIT37		(1 << 37)
#define	BIT38		(1 << 38)
#define	BIT39		(1 << 39)
#define	BIT40		(1 << 40)
#define	BIT41		(1 << 41)
#define	BIT42		(1 << 42)
#define	BIT43		(1 << 43)
#define	BIT44		(1 << 44)
#define	BIT45		(1 << 45)
#define	BIT46		(1 << 46)
#define	BIT47		(1 << 47)
#define	BIT48		(1 << 48)
#define	BIT49		(1 << 49)
#define	BIT50		(1 << 50)
#define	BIT51		(1 << 51)
#define	BIT52		(1 << 52)
#define	BIT53		(1 << 53)
#define	BIT54		(1 << 54)
#define	BIT55		(1 << 55)
#define	BIT56		(1 << 56)
#define	BIT57		(1 << 57)
#define	BIT58		(1 << 58)
#define	BIT59		(1 << 59)
#define	BIT60		(1 << 60)
#define	BIT61		(1 << 61)
#define	BIT62		(1 << 62)
#define	BIT63		(1 << 63)
#define	BIT64		(1 << 64)

/*--------------------------------- +
|									|
|	Macro							|
|									|
+ ---------------------------------*/
#define __XCAT(a,b)						(a##b)
#define	CONCAT(a,b)						(__XCAT(a,b))								/* concatenate 2 preprocessor string */
#define CONCAT3(a,b,c)					(__XCAT(__XCAT(a,b),c))						/* concatenate 3 preprocessor string */
#define	CONCAT4(a,b,c,d)				(__XCAT(__XCAT(a,b),__XCAT(c,d)))			/* concatenate 4 preprocessor string */
#define SWAP(a, b) (((a) ^= (b)), ((b) ^= (a)), ((a) ^= (b)))						/* swap in memory value a with value b */
#define FORCE_BIT(target, mask, value)	((target) ^= ((value) ^ (target)) & (mask))	/* force the value of the masked bits */
#define SET_BIT(target, mask)			((target) |= (mask))						/* force the masked bit to 1 */
#define CLR_BIT(target, mask)			((target) &= ~(mask))						/* force the masked bit to 0 */
#define TOG_BIT(target, mask)			((target) ^= (mask))						/* change the masked bit value */

#define	PREPROC_MAX(a,b)				__extension__({__typeof__(a) _a = (a); __typeof__(b) _b = (b); _a > _b ? _a : _b;})		/* taken from em_common.h */
#define	PREPROC_MIN(a,b)				__extension__({__typeof__(a) _a = (a); __typeof__(b) _b = (b); _a < _b ? _a : _b;})

#define nop()							__asm__("nop")								/* NOP instruction call */


#endif /* MXUTIL_H_ */
