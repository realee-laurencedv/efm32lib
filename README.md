## EFM32Lib
----

Firmware drivers, service and other utilities for the EFM32, EFR32 and EZR32 MCU from SiLabs.

**DEPREACATED use [tlib](https://gitlab.com/real-ee/public/tlib) instead**

### File structure

There is 3 main folder:
* **inc**: Holds headers and macro files, should be added to the compiler's include path
* **src**: Holds the actual source files, should be linked or added to the compile list of the compiler
* **conf**: Hold template file for the config headers, should be ignored by the compiler (see Setup section below)

### Setup

1. Get the package via the download link on the repository or with the [GitDependencyTool](https://git.megaxone.net/Tools/GitDependencyTool)
2. Add the *inc* folder to the compiler's include path and link/copy the *src* folder to the compile list
3. Make a copy of the *conf* folder into your project folder and add the original *conf* folder to the compiler's ignore list
	* Needed only if you use an automatic way of updating this package (it would overwrite you configuration after each update)
4. Adjust the configuration to your needs and report any problems or ideas on the issue tracker
	* All configuration can alternatively be set via symbol in your toolchain (useful for different build-configuration)

### Usage

You are free to do whatever you want with those files, just send me an email with pictures and stories if you feel like you made a great thing
with this!

### Warning

There is 2 Interrupt handler in the mxdbg.c file, namely `NMI_Handler()` and `HardFault_Handler()` which are cpu exception interpreter.
They are implemented as strong-linked-functions so they should over-ride the weakly-linked default handler in emlib. They will output
human readable string depending on the exception encountered and if a JTAG/SWD debugger is connected or not.

