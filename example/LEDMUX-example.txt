/*
*	\name		LEDMUX-example.txt
*	\author		Laurence DV
*	\date		2017-08-14
*	\version	0.1.0
*	\brief		Example usage of the LEDMUX
*	\note
*	\warning	None
*/
/*--------------------------------- +
|									|
|	Include							|
|									|
+ ---------------------------------*/
#include <stdint.h>
#include <cmsis_os.h>

/* EFM32 specifics */
#include <em_device.h>
#include <em_chip.h>
#include <em_core.h>
#include <em_cmu.h>
#include <em_gpio.h>
#include <em_timer.h>

/* Project specifics */
#include <LEDmux.h>


/*--------------------------------- +
|									|
|	Thread 							|
|									|
+ ---------------------------------*/
#define	LED_1		(0)		/* This reflect the order in which you configured the pin/port in LEDmux_conf.h ($LEDMUXcommonPins and $LEDMUXcommonPorts) */
#define	LED_2		(1)


/*--------------------------------- +
|									|
|	Thread 							|
|									|
+ ---------------------------------*/
osThreadDef(LEDMUXThread, osPriorityHigh, 1, 0);		/* 0. Thread definitions and control in main */
osThreadId LEDMUXThreadID;


/*--------------------------------- +
|									|
|	Main							|
|									|
+ ---------------------------------*/
int main(void) {
	/* Init */
	CHIP_Init();

	/* -- Clocks -- */
	CMU_HFRCOBandSet(cmuHFRCOBand_21MHz);
	CMU_ClockEnable(cmuClock_HFPER, true);
	CMU_ClockEnable(cmuClock_GPIO, true);

	/* 1. Create and start the thread */
	LEDMUXThreadID = osThreadCreate(osThread(LEDMUXThread), NULL);
	osDelay(10);		/* Just to ensure the msgBox is ready before sending msg */

	/* 2. Send msg to set the color of each LED */
	LEDMUXmsg_t testMsg;

	/* Display the color (0xff, 0x7f, 0x0f) on LED_1 */
	testMsg.LEDsel = LED_1;
	testMsg.blinkEn = 0;
	testMsg.red = 0xFF;
	testMsg.green = 0x7F;
	testMsg.blue = 0x0F;
	osMessagePut(LEDMUXmsgBox, testMsg.all, osWaitForever);

	/* Display the blinking color (0x22, 0x00, 0xd0) on LED_1 */
	testMsg.LEDsel = LED_2;
	testMsg.blinkEn = 1;
	testMsg.red = 0x22;
	testMsg.green = 0x00;
	testMsg.blue = 0xD0;
	osMessagePut(LEDMUXmsgBox, testMsg.all, osWaitForever);

	/* Infinite loop */
	while (1) {
		osDelay(100);
  	}
}


/*--------------------------------- +
|									|
|	ISR								|
|									|
+ ---------------------------------*/
void TIMER0_IRQHandler(void) {
	LEDMUX_ISR();
}
