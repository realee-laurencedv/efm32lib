/*
	\name		Megaxone's Debugging tools
	\author		Laurence DV
	\date		2017-05-23
	\version	0.3.0
	\note		Lowest level debug port for system diagnostic
	\warning	
*/
#include "mxdbg.h"
#include <em_dbg.h>

/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ ---------------------------------*/
#define __MXDBG_GPIO_MAXNBOFPINS	(16)		/* Physical maximum number of pin in a IO ports */
#define	__MXDBG_GPIO_MAXGPIO_MASK	(0xFFFF)	/* Maximum output value with GPIO Port */

/* Memory Fault */
#define __CM_CFSR_INSTACCVIOL	(BIT0)
#define	__CM_CFSR_DATAACCVIOL	(BIT1)
#define	__CM_CFSR_MEMUNSTACKING	(BIT3)
#define	__CM_CFSR_MEMSTACKING	(BIT4)
#define	__CM_CFSR_MEMFPLAZY		(BIT5)		/* Only CM-4F */
#define	__CM_CFSR_MMFARVALID	(BIT7)
/* Bus Fault */
#define	__CM_CFSR_INSTBUSERR	(BIT8)
#define	__CM_CFSR_PRECISERR		(BIT9)
#define	__CM_CFSR_IMPRECISERR	(BIT10)
#define	__CM_CFSR_BUSUNSTACKING	(BIT11)
#define	__CM_CFSR_BUSSTACKING	(BIT12)
#define	__CM_CFSR_BUSFPLAZY		(BIT13)		/* Only CM-4F */
#define	__CM_CFSR_BFARVALID		(BIT15)
/* Usage Fault */
#define	__CM_CFSR_UNDEFINST		(BIT16)
#define	__CM_CFSR_INVALIDSTATE	(BIT17)
#define	__CM_CFSR_INVALIDPC		(BIT18)
#define	__CM_CFSR_COPROCERR		(BIT19)
#define	__CM_CFSR_UNALIGNED		(BIT24)
#define	__CM_CFSR_DIVBYZERO		(BIT25)

/* -------------------------------- +
|									|
|	Global Variable					|
|									|
+ ---------------------------------*/


/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ ---------------------------------*/
#define _DDPREG						(GPIO->P[MXDBG_GPIO_DATAPORT].DOUT)
#if defined(_GPIO_P_DOUTCLR_MASK)
	#define _DDPREG_SET(x)					(GPIO->P[MXDBG_GPIO_DATAPORT].DOUTSET)
	#define _DDPREG_CLR(x)					(GPIO->P[MXDBG_GPIO_DATAPORT].DOUTCLR)
	#define _DDPREG_TOG(x)					(GPIO->P[MXDBG_GPIO_DATAPORT].DOUTTGL)
#else
	#define _DDPREG_SET(x)					(BUS_RegMaskedSet(&GPIO->P[MXDBG_GPIO_DATAPORT].DOUT, (x)))
	#define _DDPREG_CLR(x)					(BUS_RegMaskedClear(&GPIO->P[MXDBG_GPIO_DATAPORT].DOUT, (x)))
	#define _DDPREG_TOG(x)					(GPIO->P[MXDBG_GPIO_DATAPORT].DOUTTGL = (x))
#endif


#define	__MXDBG_GPIO_PREPMASK			(MXDBG_GPIO_DATAMASK>>MXDBG_GPIO_DATAALIGN)

/* Any edge */
#if MXDBG_GPIO_SYNCEDGE == MXDBG_GPIO_EDGEPOL_BOTH
	#define __MXDBG_GPIO_SYNCEDGE()		(GPIO->P[MXDBG_GPIO_SYNCPORT].DOUTTGL = MXDBG_GPIO_SYNCMASK)
/* Rising */
#elif MXDBG_GPIO_SYNCEDGE == MXDBG_GPIO_EDGEPOL_RISN
	#if defined(_GPIO_P_DOUTCLR_MASK)
		#define __MXDBG_GPIO_SYNCEDGE()		((GPIO->P[MXDBG_GPIO_SYNCPORT].DOUTCLR = MXDBG_GPIO_SYNCMASK),\
											(GPIO->P[MXDBG_GPIO_SYNCPORT].DOUTSET = MXDBG_GPIO_SYNCMASK))
	#else
		#define	__MXDBG_GPIO_SYNCEDGE()		(BUS_RegMaskedClear(&GPIO->P[MXDBG_GPIO_SYNCPORT].DOUT, MXDBG_GPIO_SYNCMASK)),\
											(BUS_RegMaskedSet(	&GPIO->P[MXDBG_GPIO_SYNCPORT].DOUT, MXDBG_GPIO_SYNCMASK))
	#endif
/* Falling */
#elif MXDBG_GPIO_SYNCEDGE == MXDBG_GPIO_EDGEPOL_FALL
	#if defined(_GPIO_P_DOUTCLR_MASK)
		#define __MXDBG_GPIO_SYNCEDGE()		((GPIO->P[MXDBG_GPIO_SYNCPORT].DOUTSET = MXDBG_GPIO_SYNCMASK),\
											(GPIO->P[MXDBG_GPIO_SYNCPORT].DOUTCLR = MXDBG_GPIO_SYNCMASK))
	#else
		#define	__MXDBG_GPIO_SYNCEDGE()		(BUS_RegMaskedSet(	&GPIO->P[MXDBG_GPIO_SYNCPORT].DOUT, MXDBG_GPIO_SYNCMASK)),\
											(BUS_RegMaskedClear(&GPIO->P[MXDBG_GPIO_SYNCPORT].DOUT, MXDBG_GPIO_SYNCMASK))
	#endif
#else
	#error "MXDBG_GPIO_SYNCEDGE has an invalid setting, use only MXDBG_GPIO_EDGEPOL_XXXX defines."
#endif


/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ ---------------------------------*/
/* large but quick */
uint16_t __MXDBG_GPIOsize2mask(uint16_t size) {
	switch (size) {
		case 1:		return 0x0001;
		case 2:		return 0x0003;
		case 3:		return 0x0007;
		case 4:		return 0x000F;
		case 5:		return 0x001F;
		case 6:		return 0x003F;
		case 7:		return 0x007F;
		case 8:		return 0x00FF;
		case 9:		return 0x01FF;
		case 10:	return 0x03FF;
		case 11:	return 0x07FF;
		case 12:	return 0x0FFF;
		case 13:	return 0x1FFF;
		case 14:	return 0x3FFF;
		case 15:	return 0x7FFF;
		case 16:	return 0xFFFF;
		default:	return 0x0000;
	}
}


/* -------------------------------- +
|									|
|	Peripheral function				|
|									|
+ ---------------------------------*/
void MXDBG_InitGPIO(void) {
	/* Sanity check */
	#if MXDBG_GPIO_DATAWIDTH > __MXDBG_GPIO_MAXNBOFPINS
		#error "MXDBG GPIO has too many pins defined"
	#endif

	/* Enable GPIO clock. */
	#if defined(_SILICON_LABS_32B_SERIES_1)
		CMU->HFBUSCLKEN0 |= CMU_HFBUSCLKEN0_GPIO;
	#else
		  CMU->HFPERCLKEN0 |= CMU_HFPERCLKEN0_GPIO;
	#endif

	/* Configure all pin as output */
	/* Data pin(s) */
	uint8_t i = 0;		/* rolling 1bit mask to scan through the defined MXDBG_GPIO_xxxxMASKs and set the */
	for (; i<8; i++) {	/* enable pins in PUSH-PULL mode */
		if (MXDBG_GPIO_DATAMASK & (0x01<<i)) {
			FORCE_BIT(GPIO->P[MXDBG_GPIO_DATAPORT].MODEL, _GPIO_P_MODEL_MODE0_MASK<<(i*4), _GPIO_P_MODEL_MODE0_PUSHPULL<<(i*4));
		}
	}
	for (i=0; i<8; i++) {
		if (MXDBG_GPIO_DATAMASK & (0x01<<(i+8))) {
			FORCE_BIT(GPIO->P[MXDBG_GPIO_DATAPORT].MODEH, _GPIO_P_MODEH_MODE8_MASK<<(i*4), _GPIO_P_MODEH_MODE8_PUSHPULL<<(i*4));
		}
	}
	/* Sync pin(s) */
	#if MXDBG_GPIO_SYNC == 1
		for (i=0; i<8; i++) {
			if (MXDBG_GPIO_SYNCMASK & (0x01<<i)) {
				FORCE_BIT(GPIO->P[MXDBG_GPIO_SYNCPORT].MODEL, _GPIO_P_MODEL_MODE0_MASK<<(i*4), _GPIO_P_MODEL_MODE0_PUSHPULL<<(i*4));
			}
		}
		for (i=0; i<8; i++) {
			if (MXDBG_GPIO_SYNCMASK & (0x01<<(i+8))) {
				FORCE_BIT(GPIO->P[MXDBG_GPIO_SYNCPORT].MODEH, _GPIO_P_MODEH_MODE8_MASK<<(i*4), _GPIO_P_MODEH_MODE8_PUSHPULL<<(i*4));
			}
		}
	#endif
	/* --------------------------- */

	/* Test all pin */
	for (i=0; i<__MXDBG_GPIO_MAXNBOFPINS; i++) {
		DBGSET(1<<i);
		#if MXDBG_GPIO_SYNC == 1
			__MXDBG_GPIO_SYNCEDGE();
		#endif
		DBGCLR(1<<i);
	}
	/* ------------ */
}

void MXDBG_InitSWO(void) {
#if	defined MXDBG_PORT_SWO

	if (DBG_Connected()) {
		DBG_SWOEnable(1);
	}

	/* Test the output */
	MXDBG_U8('!');
	/* --------------- */
#endif
}

void MXDBG_PrintGPIO(uint16_t code, uint8_t codeSize) {
	#if MXDBG_PORT_SEL == MXDBG_PORT_GPIO		
		/* input sizing */
		uint16_t codeMask = __MXDBG_GPIOsize2mask(codeSize);
		code &= codeMask;

		uint8_t bitSent = 0;
		for (; bitSent < codeSize; bitSent += MXDBG_GPIO_DATAWIDTH) {
			uint16_t temp0 = code & (__MXDBG_GPIO_PREPMASK << bitSent);		/* scan by DATAWIDTH steps */
			uint16_t temp1 = temp0 >> bitSent;								/* but keep them aligned to bit0 */
			uint16_t temp2 = temp1 << MXDBG_GPIO_DATAALIGN;

			/* Apply to GPIO register */
			FORCE_BIT(_DDPREG, MXDBG_GPIO_DATAMASK, temp2);

			/* Signal */
			#if MXDBG_GPIO_SYNC == 1
				__MXDBG_GPIO_SYNCEDGE();									/* Generate an edge on the sync pin */
			#else
				FORCE_BIT(_DDPREG_CLR, MXDBG_GPIO_DATAMASK, temp2);			/* Clear all pins of the data interface */
			#endif
		}
	#else
		nop();
	#endif
}


void MXDBG_PrintSWO(uint32_t code) {
	#if MXDBG_PORT_SEL == MXDBG_PORT_SWO
		ITM_SendChar((uint32_t)outValue);
	#else
		(void)code;		/* suppress warning */
		nop();
	#endif
}


/* -------------------------------- +
|									|
|	API Function					|
|									|
+ ---------------------------------*/
void MXDBG_Init(void) {
	#if MXDBG_PORT_SEL == MXDBG_PORT_GPIO
		MXDBG_InitGPIO();
	#elif MXDBG_PORT_SEL == MXDBG_PORT_SWO
		MXDBG_InitSWO();
	#elif MXDBG_PORT_SEL == MXDBG_PORT_NONE
		nop();
	#else
		#error "STDOUT selected port is not implemented!"
	#endif
}

void MXDBG_Code(uint32_t code, uint32_t codeSize) {
	#if MXDBG_PORT_SEL == MXDBG_PORT_GPIO
		while (codeSize > __MXDBG_GPIO_MAXNBOFPINS) {
			MXDBG_PrintGPIO(code & __MXDBG_GPIO_MAXGPIO_MASK, __MXDBG_GPIO_MAXNBOFPINS);
			codeSize -= __MXDBG_GPIO_MAXNBOFPINS;
			code >>= __MXDBG_GPIO_MAXNBOFPINS;
		}
		MXDBG_PrintGPIO(code, codeSize);
	#elif MXDBG_PORT_SEL == MXDBG_PORT_SWO
		ITM_SendChar((uint32_t)code);
	#else
		nop();
	#endif
}

void MXDBG_U8(uint8_t outValue) {
	#if MXDBG_PORT_SEL == MXDBG_PORT_GPIO
		MXDBG_PrintGPIO(outValue, 8);
	#elif MXDBG_PORT_SEL == MXDBG_PORT_SWO
		ITM_SendChar((uint32_t)outValue);
	#else
		nop();
	#endif
}

void MXDBG_U16(uint16_t outValue) {
	#if MXDBG_PORT_SEL == MXDBG_PORT_GPIO
		MXDBG_PrintGPIO(outValue, 16);
	#elif MXDBG_PORT_SEL == MXDBG_PORT_SWO
		ITM_SendChar((uint32_t)outValue);
	#else
		nop();
	#endif
}

void MXDBG_U32(uint32_t outValue) {
	#if MXDBG_PORT_SEL == MXDBG_PORT_GPIO
		MXDBG_PrintGPIO(outValue & 0xFFFF, 16);
		MXDBG_PrintGPIO(outValue >> 16 , 16);
	#elif MXDBG_PORT_SEL == MXDBG_PORT_SWO
		ITM_SendChar((uint32_t)outValue);
	#else
		nop();
	#endif
}

void MXDBG_Array(uint8_t * arrayPtr, uint32_t byteNb) {
	uint32_t byteSent = 0;
	for (byteSent=0; byteSent<byteNb; byteSent++) {
		MXDBG_U8(arrayPtr[byteSent]);
	}
}

void MXDBG_String(const char * string) {
	uint32_t i = 0;
	while(string[i] != '\0') {
		MXDBG_U8(string[i]);
		i++;
	}
}


/* -------------------------------- +
|									|
|	CPU Exception handling			|
|									|
+ ---------------------------------*/
/* HardFault data extract function */
#if defined _MXFBG_CORE_ACCESS
void _MXDBG_HardFault(uint32_t *sp) {
	char stringBuf[18];
	uint8_t breakFlag=1;
	uint8_t printCPUReg=0;

    uint32_t cfsr  = SCB->CFSR;
    uint32_t hfsr  = SCB->HFSR;
    uint32_t mmfar = SCB->MMFAR;
    uint32_t bfar  = SCB->BFAR;
    uint32_t r0  = sp[0];
    uint32_t r1  = sp[1];
    uint32_t r2  = sp[2];
    uint32_t r3  = sp[3];
    uint32_t r12 = sp[4];
    uint32_t lr  = sp[5];
    uint32_t pc  = sp[6];
    uint32_t psr = sp[7];

	MXDBG_String("Hard Fault: ");
	if (cfsr & __CM_CFSR_INSTACCVIOL) {
		MXDBG_String("Mem manager fault: Instruction access violation(faulty inst. @PC)\n");
		printCPUReg = 1;
	} else if (cfsr & __CM_CFSR_DATAACCVIOL) {
		MXDBG_String("Mem manager fault: Data access violation (faulty inst. @PC) (faulty data @MMFAR)\n");
		printCPUReg = 1;
	} else if (cfsr & __CM_CFSR_MEMUNSTACKING) {
		MXDBG_String("Mem manager fault: unstacking\n");
	} else if (cfsr & __CM_CFSR_MEMSTACKING) {
		MXDBG_String("Mem manager fault: stacking\n");
	} else if (cfsr & __CM_CFSR_MEMFPLAZY) {
		MXDBG_String("Mem manager fault: FPU lazy\n");
	} else if (cfsr & __CM_CFSR_INSTBUSERR) {
		MXDBG_String("Bus fault: instruction error\n");
	} else if (cfsr & __CM_CFSR_PRECISERR) {
		MXDBG_String("Bus fault: data error (precise) (faulty data @BFAR)\n");
	} else if (cfsr & __CM_CFSR_IMPRECISERR) {
		MXDBG_String("Bus fault: data error (imprecise)\n");
	} else if (cfsr & __CM_CFSR_BUSUNSTACKING) {
		MXDBG_String("Bus fault: unstacking\n");
	} else if (cfsr & __CM_CFSR_BUSSTACKING) {
		MXDBG_String("Bus fault: stacking\n");
	} else if (cfsr & __CM_CFSR_BUSFPLAZY) {
		MXDBG_String("Bus fault: FPU lazy\n");
	} else if (cfsr & __CM_CFSR_UNDEFINST) {
		MXDBG_String("Undefined instruction (faulty inst. @PC)\n");
	} else if (cfsr & __CM_CFSR_INVALIDSTATE) {
		MXDBG_String("EPSR rendered invalid (faulty inst. @PC)\n");
		printCPUReg = 1;
	} else if (cfsr & __CM_CFSR_INVALIDPC) {
		MXDBG_String("Invalid PC (faulty inst. @PC)\n");
		printCPUReg = 1;
	} else if (cfsr & __CM_CFSR_COPROCERR) {
		MXDBG_String("No Co-Processor\n");
	} else if (cfsr & __CM_CFSR_UNALIGNED) {
		MXDBG_String("Unaligned memory access\n");
	} else if (cfsr & __CM_CFSR_DIVBYZERO) {
		MXDBG_String("Division by 0 (faulty inst. @PC)\n");
		printCPUReg = 1;
	} else {
		MXDBG_String("\n");
		printCPUReg = 1;
	}

	sprintf(stringBuf, "HFSR:\t0x%08x\n", (unsigned int)hfsr);		MXDBG_String(stringBuf);
	sprintf(stringBuf, "CFSR:\t0x%08x\n", (unsigned int)cfsr);		MXDBG_String(stringBuf);
	if (cfsr & __CM_CFSR_MMFARVALID) {
		sprintf(stringBuf, "MMFAR:\t0x%08x\n", (unsigned int)mmfar);MXDBG_String(stringBuf);
	}
	if (cfsr & __CM_CFSR_BFARVALID) {
		sprintf(stringBuf, "BFAR:\t0x%08x\n", (unsigned int)bfar);	MXDBG_String(stringBuf);
	}
	if (printCPUReg) {
		sprintf(stringBuf, "R0:\t0x%08x\n", (unsigned int)r0);		MXDBG_String(stringBuf);
		sprintf(stringBuf, "R1:\t0x%08x\n", (unsigned int)r1);		MXDBG_String(stringBuf);
		sprintf(stringBuf, "R2:\t0x%08x\n", (unsigned int)r2);		MXDBG_String(stringBuf);
		sprintf(stringBuf, "R3:\t0x%08x\n", (unsigned int)r3);		MXDBG_String(stringBuf);
		sprintf(stringBuf, "R12:\t0x%08x\n", (unsigned int)r12);	MXDBG_String(stringBuf);
		sprintf(stringBuf, "LR:\t0x%08x\n", (unsigned int)lr);		MXDBG_String(stringBuf);
		sprintf(stringBuf, "PC:\t0x%08x\n", (unsigned int)pc);		MXDBG_String(stringBuf);
		sprintf(stringBuf, "PSR:\t0x%08x\n", (unsigned int)psr);	MXDBG_String(stringBuf);
	}
	if (lr & BIT2) {
		MXDBG_String("Was using Process stack\n");
	} else {
		MXDBG_String("Was using Main stack\n");
	}

	if (CoreDebug->DHCSR & 1) {
		//__BKPT(1); /* Halt execution if debugger connected */
		while(breakFlag);
	} else {
		MXDBG_String("Hard fault reset\n");
		NVIC_SystemReset();
	}
}
#endif

/* Non-Maskable-Interrupt handler */
#if defined _MXFBG_CORE_ACCESS
void NMI_Handler(void) {
	uint8_t breakFlag = 1;
	volatile uint32_t fault = SCB->CFSR;
	fault = fault;		/* Suppress warning but keep content for debugging inspection */

	if (CoreDebug->DHCSR & 1) {
		__BKPT(1); /* Halt execution if debugger connected */
		while(breakFlag);
	} else {
		MXDBG_String("NMI reset\n");
		NVIC_SystemReset();
	}
}
#endif

/* CPU HardFault interrupt handler */
#if defined _MXFBG_CORE_ACCESS
__attribute__( (naked) )
void HardFault_Handler(void) {
    __asm volatile (
        "tst lr, #4                                      \n"
        "ite eq                                          \n"
        "mrseq r0, msp                                   \n"
        "mrsne r0, psp                                   \n"
        "ldr r1, MXDBG_Hardfault_address                 \n"
        "bx r1                                           \n"
        "MXDBG_Hardfault_address: .word _MXDBG_HardFault \n"
    );
}
#endif

