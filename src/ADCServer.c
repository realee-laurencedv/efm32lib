/*
	\name		ADCServer.c
	\author		Laurence DV
	\date		2017-05-16
	\version	0.2.0
	\note		OS Hardware-running service used to get ADC fixed freq sampling
				This lib is thread-safe (tested with RTX v.4.70)
	\usage		See ADCServer-example.txt (not implemented yet)
	\warning	
*/
#include <ADCServer.h>

/* -------------------------------- +
|									|
|	DMA Wizardy						|
|									|
+ ---------------------------------*/
#include "dmactrl.h"

#if ( ( DMA_CHAN_COUNT > 0 ) && ( DMA_CHAN_COUNT <= 4 ) )
	#define DMACTRL_CH_CNT      4
	#define DMACTRL_ALIGNMENT   128
#elif ( ( DMA_CHAN_COUNT > 4 ) && ( DMA_CHAN_COUNT <= 8 ) )
	#define DMACTRL_CH_CNT      8
	#define DMACTRL_ALIGNMENT   256
#elif ( ( DMA_CHAN_COUNT > 8 ) && ( DMA_CHAN_COUNT <= 12 ) )
	#define DMACTRL_CH_CNT      16
	#define DMACTRL_ALIGNMENT   256
#else
	#error "Unsupported DMA channel count (dmactrl.c)."
#endif

/* DMA control block array, requires proper alignment. */
#if defined (__ICCARM__)
	#pragma data_alignment=DMACTRL_ALIGNMENT
	DMA_DESCRIPTOR_TypeDef dmaControlBlock[DMACTRL_CH_CNT * 2];
#elif defined (__CC_ARM)
	DMA_DESCRIPTOR_TypeDef dmaControlBlock[DMACTRL_CH_CNT * 2] __attribute__ ((aligned(DMACTRL_ALIGNMENT)));
#elif defined (__GNUC__)
	DMA_DESCRIPTOR_TypeDef dmaControlBlock[DMACTRL_CH_CNT * 2] __attribute__ ((aligned(DMACTRL_ALIGNMENT)));
#else
	#error Undefined toolkit, need to define alignment
#endif


/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ ---------------------------------*/
#define	_ADCS_TGRAD				(-6.3)			/* Temp-sensor grad coefficient */
#define	_ADCS_LAST_VALID_CH		(ADCS_DAC1)		/* Valid channel ID should be =< than this */


/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ ---------------------------------*/


/* -------------------------------- +
|									|
|	Private Global Variable			|
|									|
+ ---------------------------------*/
#if defined osCMSIS
osPoolDef(ADCS_Pool, ADCS_MAXNB_MODULE, ADCS_t);
osPoolId ADCS_PoolID;
#endif
static ADCS_t * __ADCS_Ctl = NULL;				/* This will not work if there is multiple instance of the ADCServer! */


/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ ---------------------------------*/
#define		_ADCS_MONCNT_TRIG		((ADCS_CONVRATE/ADCS_MONRATE))		/* Number of scan to do before trigerring conversion of the monitored internal input */
#define		_ADCS_ADC_PRS_CH(x)		CONCAT(adcPRSSELCh, x)

/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ ---------------------------------*/
static void __ADCS_StartMon(void) {
	CORE_DECLARE_IRQ_STATE;
	CORE_ENTER_ATOMIC();
	if (__ADCS_Ctl->mon.curID < __ADCS_Ctl->mon.enCnt) {
		/* Select the next analog input */
		FORCE_BIT(	ADCS_ADC_MOD->SINGLECTRL,
					_ADC_SINGLECTRL_INPUTSEL_MASK,
					(__ADCS_Ctl->mon.ch[__ADCS_Ctl->mon.curID].ID)<<_ADC_SINGLECTRL_INPUTSEL_SHIFT);
		/* Start the conversion */
		ADC_Start(ADCS_ADC_MOD, adcStartSingle);
	} else {
		DMA_ActivateBasic(ADCS_DMA_CH, true, false, __ADCS_Ctl->scan.raw, (void *)((uint32_t)&(ADCS_ADC_MOD->SCANDATA)), (ADCS_SCAN_CH_NB - 1));
	}
	CORE_EXIT_ATOMIC();
}

void __ADCS_DMAcb(unsigned int channel, bool primary, void *user) {
	(void)user;	(void)channel;	(void)primary;
	CORE_DECLARE_IRQ_STATE;
	CORE_ENTER_CRITICAL();
	__ADCS_Ctl->mon.rateCnt++;
	if (__ADCS_Ctl->mon.rateCnt >= _ADCS_MONCNT_TRIG) {
		__ADCS_Ctl->mon.rateCnt = 0;
		__ADCS_Ctl->mon.curID = 0;
		__ADCS_StartMon();		/* We just finished a scan, start the monitoring @ first channel */
	}

	DMA_ActivateBasic(ADCS_DMA_CH, true, false, __ADCS_Ctl->scan.raw, (void *)((uint32_t)&(ADCS_ADC_MOD->SCANDATA)), (ADCS_SCAN_CH_NB - 1));
	CORE_EXIT_CRITICAL();
}

static ADCS_MonCh_t * __ADCS_FindCh(uint8_t channelID) {
	uint8_t i = 0;
	for (; i<__ADCS_Ctl->mon.enCnt; i++) {
		if (__ADCS_Ctl->mon.ch[i].ID == channelID) {
			return &(__ADCS_Ctl->mon.ch[i]);
		}
	}
	return NULL;
}

static void __ADCS_safeCb(uint16_t value) {
	(void)value;
	MXDBG_PrintString("ADCS Alarm without cb\n");
}

/* -------------------------------- +
|									|
|	Function						|
|									|
+ ---------------------------------*/
/* ==== Control functions ==== */
/* =========================== */
ADCS_t * ADCS_Init(uint32_t scanChannels) {
	static bool poolCreationFlag = 0;

	/* Allocate and init control */
	#if defined osCMSIS
		/* Create the pools if needed */
		if (!poolCreationFlag) {
			poolCreationFlag = 1;
			ADCS_PoolID = osPoolCreate(osPool(ADCS_Pool));
		}
		/* Allocate the control structure */
		__ADCS_Ctl = (ADCS_t *) osPoolCAlloc(ADCS_PoolID);
	#else
		#warning "This is untested"
		__ADCS_Ctl = (ADCS_t *) calloc(sizeof(ADCS_t));
	#endif

	if (__ADCS_Ctl == NULL) {
		MXDBG_PrintString("ERR: ADCS init failed\n");
		return NULL;
	} else {
		ADC_Init_TypeDef adcConf = {
			.tailgate = true,
			.lpfMode = adcLPFilterRC,
			.ovsRateSel = adcOvsRateSel2,
			.warmUpMode = adcWarmupKeepScanRefWarm,
			.timebase = ADC_TimebaseCalc(0),
			.prescale = ADC_PrescaleCalc(ADCS_CONVCLK, 0),
		};
		const ADC_InitSingle_TypeDef adcSingleConf = {
			.acqTime = ADCS_MON_ACQCYCLENB,
			.diff = false,
			.input = adcSingleInputTemp,
			.leftAdjust = false,
			.prsEnable = false,
			.reference = adcRefVDD,
			.rep = false,
			.resolution = adcRes12Bit,
		};
#warning "TODO: Verify that the asked scan/mon-rate is doable with the current resolution"
		ADC_InitScan_TypeDef adcScanConf = {
			.acqTime = ADCS_ACQCYCLENB,
			.diff = false,
			.leftAdjust = false,
			.prsSel = _ADCS_ADC_PRS_CH(ADCS_PRS_CH),
			.rep = false,
			.resolution = adcRes12Bit,
			.input = scanChannels,
			.reference  = adcRefVDD,
			.prsEnable  = true,
		};
		const TIMER_Init_TypeDef timerConf = {
			.clkSel = timerClkSelHFPerClk,
			.count2x = false,
			.debugRun = false,
			.dmaClrAct = false,
			.mode = timerModeUp,
			.oneShot = false,
			.sync = false,
			.enable = false,
			.prescale = timerPrescale1,
		};
		static DMA_Init_TypeDef dmaInit = {
			.hprot = 0,
			.controlBlock = dmaControlBlock,
		};
		static DMA_CB_TypeDef dmaCb = {
			.cbFunc = &__ADCS_DMAcb,
			.primary = true,
			.userPtr = NULL,
		};
		static DMA_CfgDescr_TypeDef descrCfg = {
			.dstInc  = dmaDataInc2,
			.srcInc  = dmaDataIncNone,
			.size    = dmaDataSize2,
			.arbRate = dmaArbitrate1,
			.hprot   = 0,
		};
		static DMA_CfgChannel_TypeDef chnlCfg = {
			.highPri   = true,
			.enableInt = false,
			.select    = DMAREQ_ADC0_SCAN,
			.cb        = &dmaCb,
		};

		/* -- Configure DMA -- */
		CMU_ClockEnable(cmuClock_DMA, true);
		DMA_Init(&dmaInit);
		DMA_CfgChannel(ADCS_DMA_CH, &chnlCfg);
		DMA_CfgDescr(ADCS_DMA_CH, true, &descrCfg);
		NVIC_EnableIRQ(DMA_IRQn);
		/* ------------------- */

		/* -- Configure Timer -- */
		CMU_ClockEnable(ADCS_TMR_CLK, true);
		TIMER_TopSet(ADCS_TMR_MOD, CMU_ClockFreqGet(ADCS_TMR_CLK)/ADCS_CONVRATE);
		TIMER_Init(ADCS_TMR_MOD, &timerConf);
		/* --------------------- */

		/* -- Configure ADC -- */
		CMU_ClockEnable(ADCS_ADC_CLK, true);
		ADC_Reset(ADCS_ADC_MOD);					/* Reset to POR state */
		ADC_Init(ADCS_ADC_MOD, &adcConf);
		ADC_InitSingle(ADCS_ADC_MOD, &adcSingleConf);
		ADC_InitScan(ADCS_ADC_MOD, &adcScanConf);
		NVIC_EnableIRQ(ADC0_IRQn);
		/* ------------------- */

		/* -- Configure PRS -- */
		CMU_ClockEnable(cmuClock_PRS, true);
		#if ADCS_TMR_MOD == TIMER0_BASE
			PRS_SourceSignalSet(ADCS_PRS_CH, PRS_CH_CTRL_SOURCESEL_TIMER0, PRS_CH_CTRL_SIGSEL_TIMER0OF, prsEdgePos);
		#elif ADCS_TMR_MOD == TIMER1_BASE
			PRS_SourceSignalSet(ADCS_PRS_CH, PRS_CH_CTRL_SOURCESEL_TIMER1, PRS_CH_CTRL_SIGSEL_TIMER1OF, prsEdgePos);
		#elif ADCS_TMR_MOD == TIMER2_BASE
			PRS_SourceSignalSet(ADCS_PRS_CH, PRS_CH_CTRL_SOURCESEL_TIMER2, PRS_CH_CTRL_SIGSEL_TIMER2OF, prsEdgePos);
		#elif ADCS_TMR_MOD == TIMER3_BASE
			PRS_SourceSignalSet(ADCS_PRS_CH, PRS_CH_CTRL_SOURCESEL_TIMER3, PRS_CH_CTRL_SIGSEL_TIMER3OF, prsEdgePos);
		#elif ADCS_TMR_MOD == TIMER4_BASE
			PRS_SourceSignalSet(ADCS_PRS_CH, PRS_CH_CTRL_SOURCESEL_TIMER4, PRS_CH_CTRL_SIGSEL_TIMER4OF, prsEdgePos);
		#elif ADCS_TMR_MOD == TIMER5_BASE
			PRS_SourceSignalSet(ADCS_PRS_CH, PRS_CH_CTRL_SOURCESEL_TIMER5, PRS_CH_CTRL_SIGSEL_TIMER5OF, prsEdgePos);
		#else
			#error "ADC Server Timer module is invalid"
		#endif
		/* ------------------- */

		/* Start the chain */
		#if ADCS_DMA_CH == (0)
			DMA_IntEnable(DMA_IEN_CH0DONE);
		#elif ADCS_DMA_CH == (1)
			DMA_IntEnable(DMA_IEN_CH1DONE);
		#elif ADCS_DMA_CH == (2)
			DMA_IntEnable(DMA_IEN_CH2DONE);
		#elif ADCS_DMA_CH == (3)
			DMA_IntEnable(DMA_IEN_CH3DONE);
		#elif ADCS_DMA_CH == (4)
			DMA_IntEnable(DMA_IEN_CH4DONE);
		#elif ADCS_DMA_CH == (5)
			DMA_IntEnable(DMA_IEN_CH5DONE);
		#elif ADCS_DMA_CH == (6)
			DMA_IntEnable(DMA_IEN_CH6DONE);
		#elif ADCS_DMA_CH == (7)
			DMA_IntEnable(DMA_IEN_CH7DONE);
		#elif ADCS_DMA_CH == (8)
			DMA_IntEnable(DMA_IEN_CH8DONE);
		#elif ADCS_DMA_CH == (9)
			DMA_IntEnable(DMA_IEN_CH9DONE);
		#elif ADCS_DMA_CH == (10)
			DMA_IntEnable(DMA_IEN_CH10DONE);
		#else
			#error "ADC DMA channel invalid"
		#endif
	}

	/* Init control struct */
	__ADCS_Ctl->mon.enCnt = 0;
	__ADCS_Ctl->mon.ref_mV = 3300;
	__ADCS_Ctl->scan.ref_mV = 3300;

	MXDBG_PrintString("INF: ADCS init done\n");
	return __ADCS_Ctl;
}

ADCS_err_t ADCS_Remove(ADCS_t * handle) {
	#if ADCS_VERIFICATION_EN == 1
	if (handle == NULL) {
		return ADCS_err_null;
	}
	#endif

	/* Stop and reset everything */
	ADCS_Stop(handle);
	ADC_Reset(ADCS_ADC_MOD);

	/* Free memory */
	#if defined osCMSIS
		if (osOK != osPoolFree(ADCS_PoolID, handle)) {
			return ADCS_err_memory;
		}
	#else
		#warning "This is untested"
		free(handle);
	#endif
	MXDBG_PrintString("ADCS removed from memory\n");
	return ADCS_success;
}

ADCS_err_t ADCS_Start(ADCS_t * handle) {
	#if ADCS_VERIFICATION_EN == 1
	if (handle == NULL) {
		return ADCS_err_null;
	}
	#endif

	handle->mon.rateCnt = 0;
	handle->mon.curID = 0;
	ADC_IntClear(ADCS_ADC_MOD, ADC_IFC_SINGLE|ADC_IFC_SCANOF|ADC_IFC_SINGLEOF);
	ADC_IntEnable(ADCS_ADC_MOD, ADC_IEN_SINGLE|ADC_IEN_SCANOF|ADC_IEN_SINGLEOF);
	DMA_ActivateBasic(ADCS_DMA_CH, true, false, __ADCS_Ctl->scan.raw, (void *)((uint32_t)&(ADCS_ADC_MOD->SCANDATA)), (ADCS_SCAN_CH_NB - 1));
	TIMER_Enable(ADCS_TMR_MOD, true);
	MXDBG_PrintString("ADCS started\n");
	return ADCS_success;
}

ADCS_err_t ADCS_Stop(ADCS_t * handle) {
	#if ADCS_VERIFICATION_EN == 1
	if (handle == NULL) {
		return ADCS_err_null;
	}
	#endif

	TIMER_Enable(ADCS_TMR_MOD, false);
	MXDBG_PrintString("ADCS stopped\n");
	return ADCS_success;
}


/* ==== Channel functions ==== */
/* =========================== */
ADCS_err_t ADCS_MonAdd(uint8_t channel, uint16_t minValue, uint16_t maxValue, void(*cbFunction)(uint16_t)) {
	#if ADCS_VERIFICATION_EN == 1
	if (channel >= _ADCS_LAST_VALID_CH) {
		return ADCS_err_invalid;
	}
	#endif

	if (__ADCS_Ctl->mon.enCnt < ADCS_MON_CH_NB) {
		CORE_DECLARE_IRQ_STATE;
		CORE_ENTER_CRITICAL();
		{
			uint8_t nextAvailID =__ADCS_Ctl->mon.enCnt;

			__ADCS_Ctl->mon.ch[nextAvailID].state = ADCS_mon_normal;
			__ADCS_Ctl->mon.ch[nextAvailID].ID = channel;
			__ADCS_Ctl->mon.ch[nextAvailID].min = minValue;
			__ADCS_Ctl->mon.ch[nextAvailID].max = maxValue;
			__ADCS_Ctl->mon.ch[nextAvailID].raw = 0;
			if (cbFunction != NULL) {
				__ADCS_Ctl->mon.ch[nextAvailID].cb = cbFunction;
			} else {
				__ADCS_Ctl->mon.ch[nextAvailID].cb = &__ADCS_safeCb;
			}

			__ADCS_Ctl->mon.enCnt++;
		}
		CORE_EXIT_CRITICAL();
		char stringBuf[6];
		MXDBG_PrintString("ADCS monitoring ch:");
		sprintf(stringBuf, "0x%02x\n", channel);
		MXDBG_PrintString(stringBuf);
		return ADCS_success;
	}
	return ADCS_err_space;		/* Max channel reached */
}

ADCS_err_t ADCS_MonChangeLvl(uint8_t channel, uint16_t minValue, uint16_t maxValue) {
	#if ADCS_VERIFICATION_EN == 1
	if (channel >= _ADCS_LAST_VALID_CH) {
		return ADCS_err_invalid;
	}
	#endif

	ADCS_MonCh_t * wptr = __ADCS_FindCh(channel);
	if (wptr != NULL) {
		CORE_DECLARE_IRQ_STATE;
		CORE_ENTER_CRITICAL();
		{
			wptr->min = minValue;
			wptr->max = maxValue;
		}
		CORE_EXIT_CRITICAL();
		return ADCS_success;
	}
	return ADCS_err_chNotFound;
}

/* ==== Value functions ==== */
/* ========================= */
uint16_t ADCS_ScanGetRaw(uint8_t channel) {
	return __ADCS_Ctl->scan.raw[channel];
}

uint16_t ADCS_MonGetRaw(uint8_t channel) {
	ADCS_MonCh_t * wptr = __ADCS_FindCh(channel);
	if (wptr != NULL) {
		return wptr->raw;
	}
	return 0;
}

uint32_t ADCS_MonGetmV(uint8_t channel) {
	ADCS_MonCh_t * wptr = __ADCS_FindCh(channel);
	if (wptr != NULL) {
		return (uint32_t)((__ADCS_Ctl->mon.ref_mV * wptr->raw)/4096);
	}
	return 0;
}

uint32_t ADCS_ScanGetmV(uint8_t channel) {
	return (uint32_t)((__ADCS_Ctl->scan.ref_mV * __ADCS_Ctl->scan.raw[channel])/4096);
	#warning "TODO: align the scan result depending on the current resolution"
}

float ADCS_TempGet(void) {
	ADCS_MonCh_t * wptr = __ADCS_FindCh(ADCS_TEMP);
	if (wptr != NULL) {
		#warning "BUG: This is not using the correct reference voltage"
		const uint32_t temp0 = (DEVINFO->CAL & _DEVINFO_CAL_TEMP_MASK) >> _DEVINFO_CAL_TEMP_SHIFT;
		const uint32_t temp0_1v25 = (DEVINFO->ADC0CAL2 & _DEVINFO_ADC0CAL2_TEMP1V25_MASK) >> _DEVINFO_ADC0CAL2_TEMP1V25_SHIFT;
		return (temp0 - (temp0_1v25 - wptr->raw) * __ADCS_Ctl->mon.ref_mV / (4096000.0 * _ADCS_TGRAD));
	}
	return -273.15;		/* Temp sensor is not monitored, return an impossibly cold value */
}

uint16_t ADCS_VDDGet_mV(void) {
	ADCS_MonCh_t * wptr = __ADCS_FindCh(ADCS_VDDDIV3);
	if (wptr != NULL) {
		return (((uint32_t)wptr->raw) * __ADCS_Ctl->mon.ref_mV * 3) / 4096;
	}
	return 0;
}

/* -------------------------------- +
|									|
|	Interrupt Service Routine		|
|									|
+ ---------------------------------*/
void ADCS_ADC_ISR(ADCS_t * handle) {
	if (ADC_IF_SCANOF & ADC_IntGetEnabled(ADCS_ADC_MOD)) {
		ADC_IntClear(ADCS_ADC_MOD, ADC_IFC_SCANOF);
		MXDBG_PrintString("ERR: ADCS Scan OF\n");
	}
	if (ADC_IF_SINGLEOF & ADC_IntGetEnabled(ADCS_ADC_MOD)) {
		ADC_IntClear(ADCS_ADC_MOD, ADC_IFC_SINGLEOF);
		MXDBG_PrintString("ERR: ADCS Single OF\n");
	}
	if (ADC_IF_SINGLE & ADC_IntGetEnabled(ADCS_ADC_MOD)) {
		ADC_IntClear(ADCS_ADC_MOD, ADC_IFC_SINGLE);

		/* Monitor current channel */
		ADCS_MonCh_t * wptr = &(handle->mon.ch[handle->mon.curID]);
		wptr->raw = ADC_DataSingleGet(ADCS_ADC_MOD);
		#warning "TODO: align the monitor result depending on the current resolution"
		switch (wptr->state) {
			case ADCS_mon_normal: {
				if (wptr->raw < wptr->min) {
					wptr->state = ADCS_mon_alarmLow;
					wptr->cb(wptr->raw);
				} else if (wptr->raw > wptr->max) {
					wptr->state = ADCS_mon_alarmHigh;
					wptr->cb(wptr->raw);
				}
				break;
			}
			case ADCS_mon_alarmLow: {
				if (wptr->raw > wptr->min) {
					wptr->state = ADCS_mon_normal;
					wptr->cb(wptr->raw);
				}
				break;
			}
			case ADCS_mon_alarmHigh: {
				if (wptr->raw < wptr->max) {
					wptr->state = ADCS_mon_normal;
					wptr->cb(wptr->raw);
				}
				break;
			}
			default:	break;
		}
		/* ----------------------- */

		/* Monitor the next channel */
		__ADCS_Ctl->mon.curID++;
		__ADCS_StartMon();
	}
}

