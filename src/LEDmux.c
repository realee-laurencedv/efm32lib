/*
	\name		LEDmux.c
	\author		Laurence DV
	\date		2017-08-15
	\version	0.1.0
	\note		RGB LED multiplexer
*/
#include <LEDmux.h>


/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ ---------------------------------*/
#define	LEDMUX_FRAME_TOP			PREPROC_MAX( 2, ((LEDMUX_PWMFREQ_HZ / LEDMUX_FREQ_HZ) /LEDMUX_LEDNB) )		/* At least 1 frame per LEDs */
#define	LEDMUX_BLINK_TOP			PREPROC_MAX( 1, ((LEDMUX_FREQ_HZ / LEDMUX_BLINK_HZ) /2) )		/* At least 1 frame per blink state */


/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ ---------------------------------*/


/* -------------------------------- +
|									|
|	Private Global Variable			|
|									|
+ ---------------------------------*/
struct {
	osThreadId		threadId;
	uint16_t		frameCnt;
	uint8_t			currentLED;
	struct {
		uint16_t	red;
		uint16_t	green;
		uint16_t	blue;
		uint8_t		LEDID;
	}nextFrame;
	struct {
		uint16_t	red;
		uint16_t	green;
		uint16_t	blue;

		uint8_t		blinkEn:1;
		uint8_t		blankFlag:1;
		uint8_t		:6;

		uint8_t		blinkCnt;
	}LEDs[LEDMUX_LEDNB];
}_LEDmux;

osMessageQDef(LEDMUXmsgBox, LEDMUX_LEDNB, LEDmsg_t);

/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ ---------------------------------*/
#if LEDMUX_COMMON_ACTIVE == 1
	#define	LEDMUX_SELECT(i)		(GPIO_PinOutSet(LEDMUXcommonPorts[i], LEDMUXcommonPins[i]))
	#define	LEDMUX_UNSELECT(i)		(GPIO_PinOutClear(LEDMUXcommonPorts[i], LEDMUXcommonPins[i]))
#else
	#define	LEDMUX_SELECT(i)		(GPIO_PinOutClear(LEDMUXcommonPorts[i], LEDMUXcommonPins[i]))
	#define	LEDMUX_UNSELECT(i)		(GPIO_PinOutSet(LEDMUXcommonPorts[i], LEDMUXcommonPins[i]))
#endif

/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ ---------------------------------*/
void _LEDMUX_selectLED(uint8_t LEDID) {
	CORE_DECLARE_IRQ_STATE;
	CORE_ENTER_ATOMIC();
	{
		/* unselect all LEDs */
		uint8_t i;
		for (i=0; i<LEDMUX_LEDNB ;i++) {
			LEDMUX_UNSELECT(i);
		}

		/* select the desired LED */
		LEDMUX_SELECT(LEDID);
	}
	CORE_EXIT_ATOMIC();
}


/* -------------------------------- +
|									|
|	Function						|
|									|
+ ---------------------------------*/



/*--------------------------------- +
|									|
|	Thread 							|
|									|
+ ---------------------------------*/
void LEDMUXThread(void const *argument) {
	osEvent  evt;
	float PWMscaleFactor = 0.0;
	(void)argument;					/* Unused parameter. */

	/* ==== Init ==== */
	LEDMUXmsgBox = osMessageCreate(osMessageQ(LEDMUXmsgBox), osThreadGetId());
	_LEDmux.threadId = osThreadGetId();
	_LEDmux.currentLED = 0;
	_LEDmux.nextFrame.LEDID = 0;

	/* GPIO */
	{
		uint8_t i;
		/* common */
		for (i=0; i<LEDMUX_LEDNB; i++) {
			GPIO_DriveModeSet(LEDMUXcommonPorts[i], gpioDriveModeHigh);		/* common terminals @ 20mA */
			GPIO_PinModeSet(LEDMUXcommonPorts[i], LEDMUXcommonPins[i], gpioModePushPull, !((bool)LEDMUX_COMMON_ACTIVE));
		}
		/* color */
		GPIO_DriveModeSet(LEDMUXcolorPorts[0], gpioDriveModeLow);			/* color terminals @ 2mA */
		GPIO_PinModeSet(LEDMUXcolorPorts[0], LEDMUXcolorPins[0], gpioModePushPull, !((bool)LEDMUX_COLOR_ACTIVE));
		GPIO_PinModeSet(LEDMUXcolorPorts[1], LEDMUXcolorPins[1], gpioModePushPull, !((bool)LEDMUX_COLOR_ACTIVE));
		GPIO_PinModeSet(LEDMUXcolorPorts[2], LEDMUXcolorPins[2], gpioModePushPull, !((bool)LEDMUX_COLOR_ACTIVE));
	}

	/* Timer */
	{
		CMU_ClockEnable(LEDMUX_TMR_CLK, true);
		TIMER_InitCC_TypeDef timerCCInit = {
			.eventCtrl  = timerEventEveryEdge,
			.edge       = timerEdgeBoth,
			.prsSel     = timerPRSSELCh0,
			.cufoa      = timerOutputActionSet,
			.cofoa      = timerOutputActionNone,
			.cmoa       = timerOutputActionClear,
			.mode       = timerCCModePWM,
			.filter     = false,
			.prsInput   = false,
			.coist      = false,
			.outInvert  = false,
		};
		#warning "TODO: implement pin polarity selection for color channels"

		/* Set Top Value */
		uint32_t frequency = CMU_ClockFreqGet(LEDMUX_TMR_CLK)/LEDMUX_PWMFREQ_HZ;
		TIMER_TopSet(LEDMUX_TMR_MOD, frequency);
		PWMscaleFactor = ((float)TIMER_TopGet(LEDMUX_TMR_MOD)) / 255.0;		/* Input color values are 8bit max */

		/* Prep Compare Channels */
		TIMER_InitCC(LEDMUX_TMR_MOD, LEDMUX_RED_CC, &timerCCInit);
		TIMER_InitCC(LEDMUX_TMR_MOD, LEDMUX_GREEN_CC, &timerCCInit);
		TIMER_InitCC(LEDMUX_TMR_MOD, LEDMUX_BLUE_CC, &timerCCInit);
		TIMER_CompareBufSet(LEDMUX_TMR_MOD, LEDMUX_RED_CC, 0);
		TIMER_CompareBufSet(LEDMUX_TMR_MOD, LEDMUX_GREEN_CC, 0);
		TIMER_CompareBufSet(LEDMUX_TMR_MOD, LEDMUX_BLUE_CC, 0);

		/* Set timer parameters */
		TIMER_Init_TypeDef timerInit = {
			.enable     = true,
			.debugRun   = true,
			.prescale   = timerPrescale1,
			.clkSel     = timerClkSelHFPerClk,
			.fallAction = timerInputActionNone,
			.riseAction = timerInputActionNone,
			.mode       = timerModeUp,
			.dmaClrAct  = false,
			.quadModeX4 = false,
			.oneShot    = false,
			.sync       = false,
		};

		LEDMUX_TMR_MOD->ROUTE = LEDMUX_TMR_LOC|LEDMUX_TMR_CCEN;
		TIMER_IntEnable(LEDMUX_TMR_MOD, TIMER_IEN_OF);
		NVIC_EnableIRQ(LEDMUX_TMR_IRQ);
		TIMER_Init(LEDMUX_TMR_MOD, &timerInit);			/* Actual start */
	}
	/* ---------- */

	osSignalSet(osThreadGetId(), LEDMUXsig_frameDone);			/* Start the first frame immediately */

	/* ==== Thread main ==== */
	while (1) {

		osSignalWait(0, osWaitForever);		/* Wait for any signal */
		/* -- Check for new message  -- */
		evt = osMessageGet(LEDMUXmsgBox, 0);		
		if (evt.status == osEventMessage) {

			/* Parse the message */
			_LEDmux.LEDs[ ((LEDMUXmsg_t)(evt.value.v)).LEDsel ].red =	(float)(((LEDMUXmsg_t)(evt.value.v)).red) * PWMscaleFactor;
			_LEDmux.LEDs[ ((LEDMUXmsg_t)(evt.value.v)).LEDsel ].green =	(float)(((LEDMUXmsg_t)(evt.value.v)).green) * PWMscaleFactor;
			_LEDmux.LEDs[ ((LEDMUXmsg_t)(evt.value.v)).LEDsel ].blue =	(float)(((LEDMUXmsg_t)(evt.value.v)).blue) * PWMscaleFactor;
			_LEDmux.LEDs[ ((LEDMUXmsg_t)(evt.value.v)).LEDsel ].blinkEn = (((LEDMUXmsg_t)(evt.value.v)).blinkEn);

			/* Cleanup */
			//osPoolFree(mpool, evt.value.p);
		}
		/* --------------------------- */

		/* -- Select the LED -- */
		if (_LEDmux.currentLED >= (LEDMUX_LEDNB-1)) {
			_LEDmux.nextFrame.LEDID = 0;
		} else {
			_LEDmux.nextFrame.LEDID = _LEDmux.currentLED+1;
		}
		//_LEDMUX_selectLED(_LEDmux.currentLED);
		/* -------------------- */

		/* -- Blinking ctl -- */
		if (_LEDmux.LEDs[_LEDmux.nextFrame.LEDID].blinkEn) {
			_LEDmux.LEDs[_LEDmux.nextFrame.LEDID].blinkCnt++;
			if (_LEDmux.LEDs[_LEDmux.nextFrame.LEDID].blinkCnt >= LEDMUX_BLINK_TOP) {
				_LEDmux.LEDs[_LEDmux.nextFrame.LEDID].blinkCnt = 0;
				_LEDmux.LEDs[_LEDmux.nextFrame.LEDID].blankFlag ^= 1;
			}
		} else {
			_LEDmux.LEDs[_LEDmux.nextFrame.LEDID].blinkCnt = 0;
			_LEDmux.LEDs[_LEDmux.nextFrame.LEDID].blankFlag = 0;
		}
		/* ------------------ */

		/* -- Update colors -- */
		if (_LEDmux.LEDs[_LEDmux.nextFrame.LEDID].blankFlag) {
			_LEDmux.nextFrame.red = 0;
			_LEDmux.nextFrame.green = 0;
			_LEDmux.nextFrame.blue = 0;
		} else {
			_LEDmux.nextFrame.red = _LEDmux.LEDs[_LEDmux.nextFrame.LEDID].red;
			_LEDmux.nextFrame.green = _LEDmux.LEDs[_LEDmux.nextFrame.LEDID].green;
			_LEDmux.nextFrame.blue = _LEDmux.LEDs[_LEDmux.nextFrame.LEDID].blue;
		}
		/* ------------------- */
	}
}


/*--------------------------------- +
|									|
|	ISR								|
|									|
+ ---------------------------------*/
void LEDMUX_ISR(void) {

	if (TIMER_IntGetEnabled(LEDMUX_TMR_MOD) & TIMER_IF_OF) {
		/* Frame boundary */
		_LEDmux.frameCnt++;

		if (_LEDmux.frameCnt == LEDMUX_FRAME_TOP) {
			LEDMUX_UNSELECT(_LEDmux.currentLED);

			TIMER_CompareBufSet(LEDMUX_TMR_MOD, LEDMUX_RED_CC,		_LEDmux.nextFrame.red);
			TIMER_CompareBufSet(LEDMUX_TMR_MOD, LEDMUX_GREEN_CC,	_LEDmux.nextFrame.green);
			TIMER_CompareBufSet(LEDMUX_TMR_MOD, LEDMUX_BLUE_CC,		_LEDmux.nextFrame.blue);

		} else if (_LEDmux.frameCnt > LEDMUX_FRAME_TOP) {
			_LEDmux.frameCnt = 0;

			LEDMUX_SELECT(_LEDmux.nextFrame.LEDID);
			_LEDmux.currentLED = _LEDmux.nextFrame.LEDID;

			osSignalSet(_LEDmux.threadId, LEDMUXsig_frameDone);
		}
		/* -------------- */

		TIMER_IntClear(LEDMUX_TMR_MOD, TIMER_IFC_OF);
	}
}

