/*
	\name		ADCUtil.c
	\author		Laurence DV
	\date		2017-05-26
	\version	0.1.1
	\note		Various utilities for adc input, joystick and cie.
*/
#include <adcutil.h>


/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ ---------------------------------*/


/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ ---------------------------------*/
typedef struct {
	int16_t					value;
	struct {
		float				scale;
		float				__notUsed;
		int16_t				offset;
		ADCU_calibType_t	type;
		ADCU_calibState_t	state;
	}calib;
}__ADCU_joy2pts_t;

typedef struct {
	int16_t					value;
	struct {
		float				scaleLow;
		float				scaleHigh;
		uint16_t			mid;
		ADCU_calibType_t	type;
		ADCU_calibState_t	state;
	}calib;
}__ADCU_joy3pts_t;

/* -------------------------------- +
|									|
|	Private Global Variable			|
|									|
+ ---------------------------------*/


/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ ---------------------------------*/


/* -------------------------------- +
|									|
|	Private Functions				|
|									|
+ ---------------------------------*/


/* -------------------------------- +
|									|
|	Function						|
|									|
+ ---------------------------------*/
ADCU_err_t ADCU_joyCalib2pts(ADCU_joystick_t * joystick, uint16_t minValue, uint16_t maxValue, uint16_t outFullScale) {
	if (joystick == NULL) {
		return ADCU_err_null;
	}
	__ADCU_joy2pts_t * wptr = (__ADCU_joy2pts_t *)joystick;
	uint32_t ofs = ((uint32_t)outFullScale)+1;
	uint32_t ifs = (uint32_t)(maxValue-minValue);

	if (ifs != 0) {
		/* Compute coefficients */
		wptr->calib.scale = ((float)ofs) / ((float)abs(ifs));
		wptr->calib.offset = ifs/2;
		wptr->calib.type = ADCU_cal2point;
		wptr->calib.state = ADCU_stReady;
		/* -------------------- */
		return ADCU_success;
	}
	return ADCU_err_fail;
}

ADCU_err_t ADCU_joyCalib3pts(ADCU_joystick_t * joystick, uint16_t minValue, uint16_t midValue, uint16_t maxValue, uint32_t outFullScale) {
	if (joystick == NULL) {
		return ADCU_err_null;
	}

	__ADCU_joy3pts_t * wptr = (__ADCU_joy3pts_t *)joystick;
	uint32_t ofs = ((uint32_t)outFullScale)+1;
	uint32_t ifsLow = (uint32_t)(midValue-minValue);
	uint32_t ifsHigh = (uint32_t)(maxValue-midValue);

	if ((ifsLow != 0) && (ifsHigh != 0)) {
		/* Compute coefficients */
		wptr->calib.scaleLow = (((float)ofs)/2) / ((float)abs(ifsLow));
		wptr->calib.scaleHigh = (((float)ofs)/2) / ((float)abs(ifsHigh));
		wptr->calib.mid = midValue;
		wptr->calib.type = ADCU_cal3point;
		wptr->calib.state = ADCU_stReady;
		/* -------------------- */
		return ADCU_success;
	}
	return ADCU_err_fail;
}


ADCU_err_t ADCU_joyUpdate(ADCU_joystick_t * joystick, uint16_t rawValue) {
	int32_t scaledResult = 0;

	if (joystick == NULL) {
		return ADCU_err_null;
	} else if (joystick->calib.state != ADCU_stReady) {
		return ADCU_err_uncalib;
	}

	/* Update value */
	switch (joystick->calib.type) {
		case ADCU_cal2point: {
			__ADCU_joy2pts_t * wptr = (__ADCU_joy2pts_t *)joystick;
			int32_t tempOffseted = rawValue - wptr->calib.offset;
			scaledResult = ((float)tempOffseted) * wptr->calib.scale;
			break;
		}
		case ADCU_cal3point: {
			__ADCU_joy3pts_t * wptr = (__ADCU_joy3pts_t *)joystick;
			int32_t tempOffseted = rawValue - wptr->calib.mid;
			if (rawValue > (wptr->calib.mid)) {
				scaledResult = ((float)tempOffseted) * wptr->calib.scaleHigh;
			} else if (rawValue <= (wptr->calib.mid))  {
				scaledResult = ((float)tempOffseted) * wptr->calib.scaleLow;
			}
			break;
		}
		case ADCU_calNone: {
			joystick->value = rawValue;
			return ADCU_success;	/* Return immediately to skip saturation */
		}
		default:	return ADCU_err_invalid;
	}

	/* Saturate */
	if (scaledResult > INT16_MAX) {
		joystick->value = INT16_MAX;
	} else if (scaledResult < INT16_MIN) {
		joystick->value = INT16_MIN;
	} else {
		joystick->value = (int16_t)scaledResult;
	}
	/* -------- */

	return ADCU_success;
}

int16_t ADCU_joyGet(ADCU_joystick_t * joystick) {
	if (joystick == NULL) {
		return 0;
	} else if (joystick->calib.state != ADCU_stReady) {
		return 0;
	}

	return joystick->value;
}

